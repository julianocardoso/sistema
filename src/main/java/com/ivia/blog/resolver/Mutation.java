package com.ivia.blog.resolver;

import java.util.Optional;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.ivia.blog.model.Comments;
import com.ivia.blog.model.Posts;
import com.ivia.blog.repository.CommentsRepository;
import com.ivia.blog.repository.PostsRepository;

public class Mutation implements GraphQLMutationResolver {

    private PostsRepository postsRepository;
    private CommentsRepository commentsRepository;

    public Mutation(PostsRepository postsRepository, CommentsRepository commentsRepository) {
        this.postsRepository = postsRepository;
        this.commentsRepository = commentsRepository;
    }

    public Posts newPosts(String nome) {
        Posts posts = new Posts();
        posts.setNome(nome);

        postsRepository.save(posts);

        return posts;
    }

    public Comments newComments(String texto, Long posts_id) {
        Comments comments = new Comments();
        comments.setTexto(texto);
        Optional<Posts> posts = postsRepository.findById(posts_id);
        comments.setPosts(posts.get());

        commentsRepository.save(comments);

        return comments;
    }

    public Posts updatePost(String nome, Long id) {
        Optional<Posts> Oposts = this.postsRepository.findById(id);
        Posts posts = Oposts.get();
        try {
            posts.setNome(nome);
            postsRepository.save(posts);
            return posts;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public boolean deleteComments(Long id) {
        commentsRepository.deleteById(id);
        return true;
    }

}
