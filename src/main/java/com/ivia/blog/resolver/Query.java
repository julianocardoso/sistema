package com.ivia.blog.resolver;

import java.util.ArrayList;
//import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
//import java.util.Map;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.ivia.blog.model.Comments;
import com.ivia.blog.model.Posts;
import com.ivia.blog.repository.CommentsRepository;
import com.ivia.blog.repository.PostsRepository;

public class Query implements GraphQLQueryResolver {
    private PostsRepository postsRepository;
    private CommentsRepository commentsRepository;

    public Query(PostsRepository postsRepository, CommentsRepository commentsRepository) {
        this.postsRepository = postsRepository;
        this.commentsRepository = commentsRepository;
    }

    public Iterable<Posts> findAllPosts() {
        return postsRepository.findAll();
    }

    public Iterable<Posts> findAllPostsWithComments() {

        Iterable<Comments> itComments = commentsRepository.findAll();

        if (itComments == null) {
            return null;
        }

        Iterator<Comments> it = itComments.iterator();

        List<Posts> lPosts = new ArrayList<>();

        Long compare = -1L;

        while (it.hasNext()) {// coleta IDs distintos
            Posts pt = it.next().getPosts();

            if (!pt.getId().equals(compare)) {
                compare = pt.getId();

                lPosts.add(pt);
            }
        }

        System.out.println("[POSTS] " + lPosts);
        Iterable<Posts> itPosts = lPosts;

        return itPosts;

        /*
         * for (Iterable<Posts> it = itPostRep; it.iterator().hasNext();) { Posts posts
         * = it.iterator().next(); System.out.println(posts.toString()); }
         */
    }

    public Iterable<Comments> findAllComments() {
        return commentsRepository.findAll();
    }

}
