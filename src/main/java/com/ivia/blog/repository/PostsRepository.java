package com.ivia.blog.repository;

import com.ivia.blog.model.Posts;
import org.springframework.data.repository.CrudRepository;

public interface PostsRepository extends CrudRepository<Posts, Long> {
}
