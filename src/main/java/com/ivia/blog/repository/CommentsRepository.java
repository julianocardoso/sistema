package com.ivia.blog.repository;

import com.ivia.blog.model.Comments;
import org.springframework.data.repository.CrudRepository;

public interface CommentsRepository extends CrudRepository<Comments, Long> {
}
