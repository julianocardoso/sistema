package com.ivia.blog.model;

import javax.persistence.*;

@Entity
@Table(name = "comments")
public class Comments {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "texto", nullable = false)
    private String texto;

    @ManyToOne
    @JoinColumn(name = "post_id", nullable = true)
    private Posts posts;

    public Comments() {
    }

    public Comments(Posts posts, String texto) {
        this.texto = texto;
        this.posts = posts;
    }

    public Comments(Long id, String texto, Posts posts) {
        this.id = id;
        this.texto = texto;
        this.posts = posts;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTexto() {
        return this.texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Posts getPosts() {
        return this.posts;
    }

    public void setPosts(Posts posts) {
        this.posts = posts;
    }

    public Comments id(Long id) {
        this.id = id;
        return this;
    }

    public Comments texto(String texto) {
        this.texto = texto;
        return this;
    }

    public Comments posts(Posts posts) {
        this.posts = posts;
        return this;
    }

    @Override
    public String toString() {
        return "{" + " id='" + getId() + "'" + ", texto='" + getTexto() + "'" + ", posts='" + getPosts() + "'" + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Comments comments = (Comments) o;

        return id.equals(comments.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

}
