package com.ivia.blog;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ivia.blog.exception.GraphQLErrorAdapter;
import com.ivia.blog.model.Comments;
import com.ivia.blog.model.Posts;
import com.ivia.blog.repository.CommentsRepository;
import com.ivia.blog.repository.PostsRepository;
import com.ivia.blog.resolver.Mutation;
import com.ivia.blog.resolver.Query;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.servlet.GraphQLErrorHandler;

/***
 * 
 */
@SpringBootApplication
public class GraphQlApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphQlApplication.class, args);
	}

	@Bean
	public GraphQLErrorHandler errorHandler() {
		return new GraphQLErrorHandler() {
			@Override
			public List<GraphQLError> processErrors(List<GraphQLError> errors) {
				List<GraphQLError> clientErrors = errors.stream().filter(this::isClientError)
						.collect(Collectors.toList());

				List<GraphQLError> serverErrors = errors.stream().filter(e -> !isClientError(e))
						.map(GraphQLErrorAdapter::new).collect(Collectors.toList());

				List<GraphQLError> e = new ArrayList<>();
				e.addAll(clientErrors);
				e.addAll(serverErrors);
				return e;
			}

			protected boolean isClientError(GraphQLError error) {
				return !(error instanceof ExceptionWhileDataFetching || error instanceof Throwable);
			}
		};
	}

	@Bean
	public Query query(PostsRepository postsRepository, CommentsRepository commentsRepository) {
		return new Query(postsRepository, commentsRepository);
	}

	@Bean
	public Mutation mutation(PostsRepository postsRepository, CommentsRepository commentsRepository) {
		return new Mutation(postsRepository, commentsRepository);
	}

	@Bean
	public CommandLineRunner demo(PostsRepository postsRepository, CommentsRepository commentsRepository) {
		return (args) -> {
			Posts tecnologia = new Posts("Tecnologia");
			Posts religiao = new Posts("Religião");
			Posts politica = new Posts("Política");
			Posts informatica = new Posts("Informática");
			Posts esportes = new Posts("Esportes");

			postsRepository.save(tecnologia);
			postsRepository.save(religiao);
			postsRepository.save(politica);
			postsRepository.save(informatica);
			postsRepository.save(esportes);

			Comments c1 = new Comments(tecnologia, "Comentário 1");
			Comments c2 = new Comments(tecnologia, "Comentário 2");
			Comments c3 = new Comments(tecnologia, "Comentário 3");
			Comments c4 = new Comments(esportes, "Comentário 4");
			Comments c5 = new Comments(informatica, "Comentário 5");

			commentsRepository.save(c1);
			commentsRepository.save(c2);
			commentsRepository.save(c3);
			commentsRepository.save(c4);
			commentsRepository.save(c5);

		};
	}
}
