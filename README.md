# Acessando o Banco de Dados

Depois de iniciar o projeto, o acesso ao banco de dados se dará por meio do link: [http://localhost:8080/h2-console/login.jsp](http://localhost:8080/h2-console/login.jsp) 
## Dados a fornecer:
- JDBC URL: jdbc:h2:mem:testdb
- User Name: sa
- Password: <deixar em branco>

# Acesso ao GraphiQL
Através do link: [http://localhost:8080/graphiql](http://localhost:8080/graphiql)

## Exempllos de Consultas

### Query
```
{
  findAllPosts {
    id
    nome
  }
}
```

### Mutation
```
mutation {
  newPosts(
    nome: "Juliano Cardoso", 
}
```

# License
MIT
